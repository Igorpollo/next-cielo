var express = require('express');
var router = express.Router();




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/', function(req, res, next) {

  if(!req.body.Amount || !req.body.Installments || !req.body.CardNumber || !req.body.Holder || !req.body.ExpirationDate || !req.body.SecurityCode || !req.body.Brand) {
    res.json({success: false, message: 'Parametros necessarios: Amount, Installments, CardNumber, Holder, ExpirationDate, SecurityCode, Brand'})
  }

  var paramsCielo = {
    'MerchantId': '51fb84ee-0ec5-449f-90b4-108df33b55cd',
    'MerchantKey': 'URVTRUBIKCMEMHXXRFBUIENVEZFLVWFXCSVTBAYK',
    'RequestId': 'xxxxxxx', // Opcional - Identificação do Servidor na Cielo
    'sandbox': true, // Opcional - Ambiente de Testes
    'debug': false // Opcional - Exibe os dados enviados na requisição para a Cielo
  }

  var cielo = require('cielo')(paramsCielo);

  var dadosSale = {  
    "MerchantOrderId":"2014111703", // Identificação própria
    "Customer":{  
       "Name":"Comprador crédito simples"
    },
    "Payment":{  
      "Type":"CreditCard",
      "Amount": req.body.Amount,
      "Installments": req.body.Installments,
      "SoftDescriptor":"123456789ABCD",
      "CreditCard":{  
          "CardNumber": req.body.CardNumber,
          "Holder": req.body.Holder,
          "ExpirationDate": req.body.ExpirationDate,
          "SecurityCode": req.body.SecurityCode,
          "Brand": req.body.Brand
      }
    }
 }
 console.log('CHAMADA CIELO');
 cielo.creditCard.simpleTransaction(dadosSale)
     .then((data) => {
       console.log(data);
       axios.post('url', { // url backend Nextfarma
         parcelas: req.body.Installments
         // demais campos
       })
       res.send('Transação criada com sucesso')
     })
     .catch(function(err){
       console.log(err);
      res.send('Erro');
	})
});


module.exports = router;
